# easy_kdb

Project to test creating KDB+ databases with a simple C API that can be loaded with the Q language.

Currently this can create a KDB+ table with ints that can be loaded in Q using:

    load `table

Where table is replaced with the name of the file saved to disk (and it
will be the name of the table).

The idea is to have a set of functions that are much simpler and make
more sense than KDB+'s C API and hopefully also be faster.  After the
table is created, it can be loaded into KDB+ to do math operations.

Update: I added the ability to create splayed tables. Splayed tables
in easy_kdb take almost no RAM, they are written directly to disk.
To load the sample splayed table after running the make_splayed_table
program:

    ./make_splayed_table testing testing_splayed
    testing: get `testing
    sym: get `testing_splayed/sym
    testing

