/**
 *  Easy KDB
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2019-2020 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "easy_kdb.h"

int main(int argc, char *argv[])
{
  kdb_splayed_table_t *table;
  const char *name;
  const char *sym_dir;

  if (argc != 3)
  {
    printf("%s <name> <table_splayed_dir>\n", argv[0]);
    exit(0);
  }

  name = argv[1];
  sym_dir = argv[2];

  // Create a new table with 4 columns.
  table = easy_kdb_splayed_table_new(4);

  // Define the column names and types.
  easy_kdb_splayed_column(table, 0, "sequence", KDB_TYPE_LONG);
  easy_kdb_splayed_column(table, 1, "price",    KDB_TYPE_INT);
  easy_kdb_splayed_column(table, 2, "symbol",   KDB_TYPE_ENUM);
  easy_kdb_splayed_column(table, 3, "comment",  KDB_TYPE_ENUM);

  // Open the files for writing.
  easy_kdb_splayed_table_open(table, name, sym_dir);

  // The indexes are in order so is possible to ignore the return value
  // and use the known value. For example MSFT is index 2.
  // Splayed tables seem to not be able to have "symbols" (aka strings)
  // as column types, so they have to be enumerated in another list.
  int enum_aapl = easy_kdb_splayed_table_enum(table, "AAPL");
  int enum_goog = easy_kdb_splayed_table_enum(table, "GOOG");
  int enum_msft = easy_kdb_splayed_table_enum(table, "MSFT");
  int enum_amzn = easy_kdb_splayed_table_enum(table, "AMZN");
  int enum_blah = easy_kdb_splayed_table_enum(table, "BLAH");
  int enum_1234 = easy_kdb_splayed_table_enum(table, "1234");

  // Add 4 rows to the table.
  easy_kdb_splayed_table_add(table, 1, 200, enum_aapl, enum_blah);
  easy_kdb_splayed_table_add(table, 2, 100, enum_goog, enum_1234);
  easy_kdb_splayed_table_add(table, 3, 400, enum_msft, enum_blah);
  easy_kdb_splayed_table_add(table, 4, 100, enum_amzn, enum_1234);

  // Close the database and free structures.
  easy_kdb_splayed_table_free(table);

  return 0;
}

