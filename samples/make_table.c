/**
 *  Easy KDB
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2019 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "easy_kdb.h"

int main(int argc, char *argv[])
{
  struct _kdb_table *kdb_table;
  int n;

  if (argc != 2)
  {
    printf("%s <outfile>\n", argv[0]);
    exit(0);
  }

  kdb_table = easy_kdb_table_new();
  easy_kdb_table_add_column(kdb_table, "sequence", KDB_TYPE_INT);
  easy_kdb_table_add_column(kdb_table, "price", KDB_TYPE_INT);
  easy_kdb_table_add_column(kdb_table, "volume", KDB_TYPE_INT);

  for (n = 0; n < 20; n++)
  {
    easy_kdb_table_append_int(kdb_table, 0, n * 10);
    easy_kdb_table_append_int(kdb_table, 1, n * 200);
    easy_kdb_table_append_int(kdb_table, 2, n * 3000);
  }

  easy_kdb_table_write(kdb_table, argv[1]);

  return 0;
}

