/**
 *  Easy KDB
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2019-2020 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>

#include "easy_kdb.h"
#include "kdb_column.h"
#include "string_pool.h"

static void write_int8(FILE *fp, int value)
{
  putc(value & 0xff, fp);
}

static void write_int16(FILE *fp, int value)
{
  putc(value & 0xff, fp);
  putc((value >> 8) & 0xff, fp);
}

static void write_int32(FILE *fp, int value)
{
  putc(value & 0xff, fp);
  putc((value >> 8) & 0xff, fp);
  putc((value >> 16) & 0xff, fp);
  putc((value >> 24) & 0xff, fp);
}

kdb_table_t *easy_kdb_table_new()
{
  kdb_table_t *kdb_table = (kdb_table_t *)malloc(sizeof(kdb_table_t));

  memset(kdb_table, 0, sizeof(kdb_table_t));

  return kdb_table;
}

void easy_kdb_table_free(kdb_table_t *kdb_table)
{
  string_pool_free(&kdb_table->string_pool);

  free(kdb_table);
}

int easy_kdb_table_add_column(
  kdb_table_t *kdb_table,
  const char *name,
  int kdb_type)
{
  int index = kdb_table->column_count++;

  kdb_column_t *column = &kdb_table->column[index];

  column->next = NULL;
  column->count = 0;
  column->list = NULL;
  column->list_last = NULL;
  column->name = string_pool_get(&kdb_table->string_pool, name, strlen(name));
  column->type = kdb_type;

  return 0;
}

int easy_kdb_table_append_int(kdb_table_t *kdb_table, int index, int value)
{
  kdb_column_t *column = &kdb_table->column[index];
  kdb_column_int_t *list = (kdb_column_int_t *)column->list_last;

  column->count++;

  if (column->list == NULL)
  {
    column->list_last = malloc(sizeof(kdb_column_int_t) + COLUMN_MAX * sizeof(int));
    column->list = column->list_last;

    list = column->list_last;
    list->next = NULL;
    list->count = COLUMN_MAX;
    list->ptr = 0;
  }

  list = column->list_last;

  if (list->ptr == list->count)
  {
    kdb_column_int_t *list_last = list;

    column->list_last = malloc(sizeof(kdb_column_int_t) + COLUMN_MAX * sizeof(int));
    list_last->next = column->list_last;

    column->list = column->list_last;

    list = column->list_last;
    list->next = NULL;
    list->count = COLUMN_MAX;
    list->ptr = 0;

    //list = (kdb_column_int_t *)column->list_last;
  }

  list->data[list->ptr++] = value;

  return 0;
}

int easy_kdb_table_write(kdb_table_t *kdb_table, const char *filename)
{
  int index, n;

  FILE *fp = fopen(filename, "wb");

  if (fp == NULL)
  {
    printf("Couldn't open output file %s.\n", filename);
    return -1;
  }

  // Write header.
  write_int8(fp, 0xff);
  write_int8(fp, 0x01);

  write_int16(fp, KDB_TYPE_TABLE);
  write_int8(fp, 0x63);
  write_int16(fp, KDB_TYPE_SYMBOL);

  write_int32(fp, kdb_table->column_count);

  for (index = 0; index < kdb_table->column_count; index++)
  {
    kdb_column_t *column = &kdb_table->column[index];

    fprintf(fp, "%s%c", column->name, 0);
  }

  // Not sure why.
  write_int16(fp, 0);
  write_int32(fp, kdb_table->column_count);

  for (index = 0; index < kdb_table->column_count; index++)
  {
    kdb_column_t *column = &kdb_table->column[index];

    write_int16(fp, column->type);
    write_int32(fp, column->count);

    kdb_column_int_t *list = (kdb_column_int_t *)column->list;

    while (list != NULL)
    {
      for (n = 0; n < list->ptr; n++)
      {
        write_int32(fp, list->data[n]);
      }

      list = list->next;
    }
  }

  fclose(fp);

  return 0;
}

int easy_kdb_table_dump(kdb_table_t *kdb_table)
{
  int index, n;

  for (index = 0; index < kdb_table->column_count; index++)
  {
    kdb_column_t *column = &kdb_table->column[index];

    printf("%s\n", column->name);

    kdb_column_int_t *list = (kdb_column_int_t *)column->list;

    while (list != NULL)
    {
      for (n = 0; n < list->ptr; n++)
      {
        printf("  %d\n", list->data[n]);
      }

      list = list->next;
    }
  }

  return 0;
}

kdb_splayed_table_t *easy_kdb_splayed_table_new(int column_count)
{
  kdb_splayed_table_t *table;
  int column_size = sizeof(kdb_splayed_column_t) * column_count;

  table = (kdb_splayed_table_t *)malloc(sizeof(kdb_splayed_table_t));
  memset(table, 0, sizeof(kdb_splayed_table_t));

  table->columns = (kdb_splayed_column_t *)malloc(column_size);
  memset(table->columns, 0, column_size);

  table->column_count = column_count;

  return table;
}

int easy_kdb_splayed_column(
  kdb_splayed_table_t *table,
  int index,
  const char *name,
  int kdb_type)
{
  kdb_splayed_column_t *column = &table->columns[index];

  column->kdb_type = kdb_type;

  snprintf(column->name, sizeof(column->name), "%s", name);

  if (kdb_type != KDB_TYPE_INT &&
      kdb_type != KDB_TYPE_LONG &&
      kdb_type != KDB_TYPE_ENUM)
  {
    return -1;
  }

  return 0;
}

int easy_kdb_splayed_table_open(
  kdb_splayed_table_t *table,
  const char *table_name,
  const char *splayed_dir)
{
  char filename[1024];
  int n, i;

  // Make needed directories.
  mkdir(table_name, 0775);
  mkdir(splayed_dir, 0775);

  // Create sym file.
  snprintf(filename, sizeof(filename), "%s/sym", splayed_dir);
  table->fp_sym = fopen(filename, "wb+");

  if (table->fp_sym == NULL) { return -1; }

  uint8_t header_sym[] = { 0xff, 0x01, 0x0b, 0x00,  0x00, 0x00, 0x00, 0x00 };
  fwrite(header_sym, sizeof(header_sym), 1, table->fp_sym);

  // Create .d file.
  snprintf(filename, sizeof(filename), "%s/.d", table_name);
  table->fp_d = fopen(filename, "wb+");

  if (table->fp_d == NULL) { return -1; }

  uint8_t header_d[] = { 0xff, 0x01, 0x0b, 0x00 };
  fwrite(header_d, sizeof(header_d), 1, table->fp_d);
  fwrite(&table->column_count, sizeof(int), 1, table->fp_d);

  for (n = 0; n < table->column_count; n++)
  {
    const char *name = table->columns[n].name;
    fwrite(name, 1, strlen(name) + 1, table->fp_d);
  }

  fclose(table->fp_d);

  // Create files for table columns.
  for (n = 0; n < table->column_count; n++)
  {
    kdb_splayed_column_t *column = &table->columns[n];

    snprintf(filename, sizeof(filename), "%s/%s", table_name, column->name);
    column->fp = fopen(filename, "wb+");

    if (column->fp == NULL) { return -1; }

    if (column->kdb_type == KDB_TYPE_ENUM)
    {
      uint8_t header[] =
      {
        0xfd, 0x20, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00,
        0x00, 0x00, 0x00, 0x00,  0x00, 0x00, 0x00, 0x00,
        's',   'y',  'm', 0x00,  0x00, 0x00, 0x00, 0x00
      };

      fwrite(header, sizeof(header), 1, column->fp);

      for (i = 0; i < 4056; i++) { putc(0x00, column->fp); }

      uint8_t header_2[] = { 0xfd, 0x00, 0x14, 0x00,  0x00, 0x00, 0x00, 0x00 };
      fwrite(header_2, sizeof(header_2), 1, column->fp);
    }
      else
    {
      putc(0xfe, column->fp);
      putc(0x20, column->fp);
      putc(column->kdb_type, column->fp);
      putc(0x00, column->fp);

      putc(0x00, column->fp);
      putc(0x00, column->fp);
      putc(0x00, column->fp);
      putc(0x00, column->fp);
    }

    uint64_t length = 0;
    fwrite(&length, sizeof(length), 1, column->fp);
  }

  return 0;
}

uint64_t easy_kdb_splayed_table_enum(
  kdb_splayed_table_t *table,
  const char *name)
{
  fwrite(name, 1, strlen(name) + 1, table->fp_sym);

  return table->index_sym++;
}

uint64_t easy_kdb_splayed_table_enum_chars(
  kdb_splayed_table_t *table,
  const char *name,
  int length)
{
  fwrite(name, 1, length, table->fp_sym);
  putc(0, table->fp_sym);

  return table->index_sym++;
}

int easy_kdb_splayed_table_add(kdb_splayed_table_t *table, ...)
{
  va_list argp;
  int n;

  va_start(argp, table);

  // If the number of paramters passed in doesn't match the column count
  // return an error.
#if 0
  if (length != table->column_count)
  {
    return -1;
  }
#endif

  int ret = 0;

  for (n = 0; n < table->column_count; n++)
  {
    kdb_splayed_column_t *column = &table->columns[n];

    if (column->kdb_type == KDB_TYPE_INT)
    {
      int value = va_arg(argp, int);
      fwrite(&value, sizeof(int), 1, table->columns[n].fp);
    }
      else
    if (column->kdb_type == KDB_TYPE_LONG ||
        column->kdb_type == KDB_TYPE_ENUM)
    {
      uint64_t value = va_arg(argp, uint64_t);
      fwrite(&value, sizeof(uint64_t), 1, table->columns[n].fp);
    }

    column->count++;
  }

  va_end(argp);

  return ret;
}

void easy_kdb_splayed_table_free(kdb_splayed_table_t *table)
{
  int n;

  for (n = 0; n < table->column_count; n++)
  {
    kdb_splayed_column_t *column = &table->columns[n];
    FILE *fp = column->fp;
    long marker = ftell(fp);

    if (column->kdb_type == KDB_TYPE_ENUM)
    {
      fseek(fp, 0xff8, SEEK_SET);
    }
      else
    {
      fseek(fp, 8, SEEK_SET);
    }

    fwrite(&column->count, sizeof(uint64_t), 1, fp);

    fseek(fp, marker, SEEK_SET);
    fclose(fp);
  }

  fclose(table->fp_sym);

  free(table);
}

