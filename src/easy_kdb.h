/**
 *  Easy KDB
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2019-2020 by Michael Kohn
 *
 */

#ifndef EASY_KDB
#define EASY_KDB

#include <stdarg.h>

#include "kdb_table.h"
#include "kdb_types.h"

kdb_table_t *easy_kdb_table_new();

void easy_kdb_table_free(kdb_table_t *kdb_table);

int easy_kdb_table_add_column(
  kdb_table_t *kdb_table,
  const char *name,
  int kdb_type);

int easy_kdb_table_append_int(
  kdb_table_t *kdb_table,
  int index,
  int value);

int easy_kdb_table_write(kdb_table_t *kdb_table, const char *filename);

kdb_splayed_table_t *easy_kdb_splayed_table_new(int column_count);

int easy_kdb_splayed_column(
  kdb_splayed_table_t *table,
  int index,
  const char *name,
  int kdb_type);

int easy_kdb_splayed_table_open(
  kdb_splayed_table_t *table,
  const char *table_name,
  const char *splayed_dir);

// The following functions require easy_kdb_splayed_table_open() to be
// called first.
uint64_t easy_kdb_splayed_table_enum(
  kdb_splayed_table_t *table,
  const char *name);

uint64_t easy_kdb_splayed_table_enum_chars(
  kdb_splayed_table_t *table,
  const char *name,
  int length);

int easy_kdb_splayed_table_add(kdb_splayed_table_t *table, ...);
void easy_kdb_splayed_table_free(kdb_splayed_table_t *table);

#endif

