/**
 *  Easy KDB
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2019 by Michael Kohn
 *
 */

#ifndef EASY_KDB_COLUMN
#define EASY_KDB_COLUMN

#include <stdint.h>

#define COLUMN_MAX 100000

typedef struct _kdb_column
{
  struct _kdb_column *next;
  uint64_t count;
  void *list;
  void *list_last;
  const char *name;
  int type;
} kdb_column_t;

typedef struct _kdb_column_int
{
  struct _kdb_column_int *next;
  int count;
  int ptr;
  int data[];
} kdb_column_int_t;

#endif

