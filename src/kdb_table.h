/**
 *  Easy KDB
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2019-2020 by Michael Kohn
 *
 */

#ifndef EASY_KDB_TABLE
#define EASY_KDB_TABLE

#include "kdb_column.h"
#include "string_pool.h"

// FIXME: 128 columns max.
#define KDB_MAX_COLUMNS 128

typedef struct _kdb_table
{
  int column_count;
  string_pool_t string_pool;
  kdb_column_t column[KDB_MAX_COLUMNS];
} kdb_table_t;

typedef struct _kdb_splayed_column
{
  FILE *fp;
  int kdb_type;
  uint64_t count;
  char name[128];
} kdb_splayed_column_t;

typedef struct _kdb_splayed_table
{
  int column_count;
  kdb_splayed_column_t *columns;
  FILE *fp_sym;
  FILE *fp_d;
  int index_sym;
} kdb_splayed_table_t;

#endif

