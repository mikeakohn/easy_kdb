/**
 *  Easy KDB
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2019-2020 by Michael Kohn
 *
 */

#ifndef EASY_KDB_TYPES
#define EASY_KDB_TYPES

#define KDB_TYPE_BOOLEAN 1
#define KDB_TYPE_GUID 2
#define KDB_TYPE_BYTE 4
#define KDB_TYPE_SHORT 5
#define KDB_TYPE_INT 6
#define KDB_TYPE_LONG 7
#define KDB_TYPE_REAL 8
#define KDB_TYPE_FLOAT 9
#define KDB_TYPE_CHAR 10
#define KDB_TYPE_SYMBOL 11
#define KDB_TYPE_TIMESTAMP 12
#define KDB_TYPE_MONTH 13
#define KDB_TYPE_DATE 14
#define KDB_TYPE_DATETIME 15
#define KDB_TYPE_TIMESPAN 16
#define KDB_TYPE_MINUTE 17
#define KDB_TYPE_SECOND 18
#define KDB_TYPE_TIME 19
#define KDB_TYPE_ENUM 20
#define KDB_TYPE_TABLE 0x62
#define KDB_TYPE_DICTIONARY 0x63

#endif

