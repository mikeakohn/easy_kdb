/**
 *  Easy KDB
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2019 by Michael Kohn
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "string_pool.h"

static string_pool_data_t *new_pool()
{
  string_pool_data_t *data;

  data = (string_pool_data_t *)malloc(sizeof(string_pool_data_t));
  data->next = NULL;
  data->ptr = 0;
  data->size = sizeof(string_pool_data_t);;

  return data;
}

const char *string_pool_get(string_pool_t *string_pool, const char *value, int length)
{
  string_pool_data_t *data = string_pool->data;

  if (data == NULL)
  {
    string_pool->data = new_pool();

    data = string_pool->data;
  }

  if (data->ptr + length + 1 > data->size)
  {
    data = new_pool();
    data->next = string_pool->data;
    string_pool->data = data;
  }

  char *temp = data->buffer + data->ptr;

  memcpy(temp, value, length);
  temp[length] = 0;

  data->ptr += length + 1;
 
  return temp; 
}

void string_pool_free(string_pool_t *string_pool)
{
  string_pool_data_t *data = string_pool->data;
  string_pool->data = NULL;

  while (data != NULL)
  {
    string_pool_data_t *temp = data;
    free(data);
    data = temp;
  }
}

