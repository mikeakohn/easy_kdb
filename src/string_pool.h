/**
 *  Easy KDB
 *  Author: Michael Kohn
 *   Email: mike@mikekohn.net
 *     Web: http://www.mikekohn.net/
 * License: GPLv3
 *
 * Copyright 2019 by Michael Kohn
 *
 */

#ifndef STRING_POOL
#define STRING_POOL

typedef struct _string_pool_data
{
  struct _string_pool_data *next;
  int size;
  int ptr;
  char buffer[512 * 1024];
} string_pool_data_t;

typedef struct _string_pool
{
  string_pool_data_t *data;
} string_pool_t;

const char *string_pool_get(string_pool_t *string_pool, const char *value, int length);
void string_pool_free(string_pool_t *string_pool);

#endif

